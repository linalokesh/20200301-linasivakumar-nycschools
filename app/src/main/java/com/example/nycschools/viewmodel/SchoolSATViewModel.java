package com.example.nycschools.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nycschools.model.SchoolSAT;
import com.example.nycschools.servercall.APIService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolSATViewModel extends ViewModel {

    //this is the data to fetch asynchronously
    private MutableLiveData<ArrayList<SchoolSAT>> schoolDetailsList;
    //Method to get data
    public LiveData<ArrayList<SchoolSAT>> getSchoolDetails(String dbn) {
        //if the arraylist is null
        if (schoolDetailsList == null) {
            schoolDetailsList = new MutableLiveData<>();
            //load it asynchronously from server in this method
            loadSchoolDetails(dbn);
        }
        //finally return the arraylist
        return schoolDetailsList;
    }

    //This method is using Retrofit to get the JSON data from URL and supply the query string as dbn
    private void loadSchoolDetails(String dbn) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService api = retrofit.create(APIService.class);
        Call<ArrayList<SchoolSAT>> call = api.getSchoolDetails(dbn);

        call.enqueue(new Callback<ArrayList<SchoolSAT>>() {
            @Override
            public void onResponse(Call<ArrayList<SchoolSAT>> call, Response<ArrayList<SchoolSAT>> response) {

                //finally set the list to  MutableLiveData
                schoolDetailsList.setValue(response.body());

            }

            @Override
            public void onFailure(Call<ArrayList<SchoolSAT>> call, Throwable t) {

                schoolDetailsList.setValue(null);


            }
        });
    }

}
