package com.example.nycschools.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolSAT;
import com.example.nycschools.viewmodel.SchoolSATViewModel;

import java.util.ArrayList;

public class SchoolDetailsActivity extends AppCompatActivity {
    TextView textViewSchoolName;
    TextView textViewSchoolLocation;
    TextView textViewSatScoresLbl;
    TextView textViewCriticalReading;
    TextView textViewMathAvg;
    TextView textViewWritingAvg;

    private String location;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_details);

        // Extracting data received through the intent
        Intent intent = getIntent();
        String dbn = intent.getStringExtra("DBN");
        location = intent.getStringExtra("LOCATION");
        name = intent.getStringExtra("NAME");
        Log.v("myLogs", "DBN " + dbn);

        //Defining textviews to display school data and sat scores

        textViewSchoolName = findViewById(R.id.txtSchoolName);
        textViewSchoolLocation = findViewById(R.id.txtSchoolLocation);
        textViewSatScoresLbl = findViewById(R.id.txtSatScoresLabel);
        textViewCriticalReading = findViewById(R.id.txtCriticalReadingAvg);
        textViewMathAvg = findViewById(R.id.txtMathAvg);
        textViewWritingAvg = findViewById(R.id.txtWritingAvg);

        // Calling the retrofit initialization method from the School model and retrieving the data into arraylist

        final SchoolSATViewModel schoolSATViewModel = ViewModelProviders.of(this).get(SchoolSATViewModel.class);
        schoolSATViewModel.getSchoolDetails(dbn).observe(this, new Observer<ArrayList<SchoolSAT>>() {
            @Override
            public void onChanged(@Nullable ArrayList<SchoolSAT> schoolDetails) {

                // Assigning appropriate text if the no sat scores are found for the selected school in the main activity

                if (schoolDetails.isEmpty()){

                    textViewSchoolName.setText(name);
                    location = location.split("\\(")[0];
                    textViewSchoolLocation.setText(location);
                    textViewSatScoresLbl.setText(R.string.no_sat_data);
                    textViewCriticalReading.setVisibility(View.GONE);
                    textViewMathAvg.setVisibility(View.GONE);
                    textViewWritingAvg.setVisibility(View.GONE);
                    Log.v("mylogs", "onChanged: in empty");


                } else {

                    // Assiging corresponding text  to the textviews when sat scores are found for the selected school in the main activity
                    String schoolName = schoolDetails.get(0).getSchoolName();
                    String criticalReadingAvgScore = schoolDetails.get(0).getSatCriticalReadingAvgScore();
                    String satMathAvgScore = schoolDetails.get(0).getSatMathAvgScore();
                    String satWritingAvgScore = schoolDetails.get(0).getSatWritingAvgScore();
                    textViewSchoolName.setText(schoolName);
                    location = location.split("\\(")[0];
                    textViewSchoolLocation.setText(location);
                    textViewSatScoresLbl.setText("sat_scores");
                  //  String criticalReadingLabel = "critical_reading_label";
                    textViewCriticalReading.setText("critical_reading_label"+(criticalReadingAvgScore));
                    textViewMathAvg.setText("SAT MathAvgScore"+(satMathAvgScore));
                    textViewWritingAvg.setText("satWritingAvgScore"+(satWritingAvgScore));
                    Log.v("mylogs", "onChanged: not empty");

                }



            }
        });
    }
}




