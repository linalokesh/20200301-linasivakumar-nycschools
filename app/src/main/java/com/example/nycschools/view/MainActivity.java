package com.example.nycschools.view;




import android.os.Bundle;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;
import com.example.nycschools.adapter.SchoolAdapter;
import com.example.nycschools.model.School;
import com.example.nycschools.viewmodel.SchoolViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SchoolAdapter adapter;

    private final ArrayList<School> schoolList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        setupRecyclerView();
//MyViewModel model = new ViewModelProvider(this).get(MyViewModel.class);
        // Calling the retrofit initialization method from the School model and retrieving the data into arraylist
        SchoolViewModel schoolViewModel = ViewModelProviders.of(this).get(SchoolViewModel.class);
        schoolViewModel.getSchools().observe(this, new Observer<ArrayList<School>>() {
            @Override
            public void onChanged(@Nullable ArrayList<School> schools) {
                schoolList.addAll(schools);
                adapter.notifyDataSetChanged();
            }
        });

    }


    // Method to setup recycler view wit the help of School Adapter

    private void setupRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager
                (this, LinearLayoutManager.VERTICAL, false);
        if (adapter == null) {
            adapter = new SchoolAdapter(this, schoolList);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setNestedScrollingEnabled(true);
        } else {
            adapter.notifyDataSetChanged();
        }
    }




}

